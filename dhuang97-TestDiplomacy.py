from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    def test_diplomacy_1(self):
        r = StringIO("A Madrid Hold\nB London Support C\nC Paris Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC Madrid\n")

    def test_diplomacy_2(self):
        r = StringIO("A Madrid Move London\nB London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\n")

    def test_diplomacy_3(self):
        r = StringIO("A Madrid Support B\nB London Move Barcelona\nC Barcelona Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\n")

if __name__ == "__main__":
    main()